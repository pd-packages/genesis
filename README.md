# JCMG Genesis

[![NPM package](https://img.shields.io/npm/v/com.playdarium.unity.genesis?logo=npm&logoColor=fff&label=NPM+package&color=limegreen)](https://www.npmjs.com/package/com.playdarium.unity.genesis)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Installing

Using the native Unity Package Manager introduced in 2017.2, you can add this library as a package by modifying your
`manifest.json` file found at `/ProjectName/Packages/manifest.json` to include it as a dependency. See the example below
on how to reference it.

### Install via OpenUPM

The package is available on the [npmjs](https://www.npmjs.com/package/com.playdarium.unity.genesis/)
registry.

#### Add registry scope

```
{
  "dependencies": {
    ...
  },
  "scopedRegistries": [
    {
      "name": "Playdarium",
      "url": "https://registry.npmjs.org",
      "scopes": [
        "com.playdarium.unity"
      ]
    }
  ]
}
```

#### Add package in PackageManager

Open `Window -> Package Manager` choose `Packages: My Regestries` and install package

### Install via GIT URL

```
"com.playdarium.unity.genesis": "https://gitlab.com/pd-packages/genesis.git#upm"
```

## Usage

To learn more about how to use JCMG Genesis, see the
wiki [here](https://github.com/jeffcampbellmakesgames/Genesis/wiki/Usage) for more information.

## Support

If this is useful to you and/or you’d like to see future development and more tools in the future, please consider
supporting it either by contributing to the Github projects (submitting bug reports or features and/or creating pull
requests) or by buying me coffee using any of the links below. Every little bit helps!

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I3I2W7GX)
